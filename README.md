## I have an interview with a bunch of XP snobs! Panik!?

> Here the best advice I can give is that, even if you dislike the full story of
> extreme programming, you should still consider seriously three technical
> practices: continuous integration, test driven development, and refactoring.
>
> -- <cite>Martin Fowler</cite>

1. [TDD](https://www.amazon.com/gp/product/0321146530?ie=UTF8&tag=martinfowlerc-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0321146530)

    - Three steps repeatedly (Red, Green, Refactor)
    - Techniques (e.g. Obvious Implementation, Fake it, Triangulate)
    - [Kent Beck's TDD Screencasts](https://pragprog.com/screencast/v-kbtdd/test-driven-development)
    - [What is a unit test in the context of TDD?](https://martinfowler.com/bliki/UnitTest.html)
    - [Classical vs Mockist TDD](https://martinfowler.com/articles/mocksArentStubs.html)

2. [Refactoring](https://martinfowler.com/books/refactoring.html)

    > The most common way that I hear to screw up TDD is neglecting the third
    > step. Refactoring the code to keep it clean is a key part of the process,
    > otherwise you just end up with a messy aggregation of code fragments.
    >
    > -- <cite>Martin Fowler</cite>

    - Code Smells (e.g. Long Method, Large Class, Switch Case ...)
    - Techniques (e.g. Extract Method, Extract Class, Replace Conditional with
      Polymorphism ...)

3. [CI](https://martinfowler.com/articles/continuousIntegration.html)

    - Integrating continuously (one or more times per day)
    - [How feature branching affects CI](https://martinfowler.com/bliki/FeatureBranch.html)
